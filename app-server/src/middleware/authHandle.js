import log4js from '../logger'
import * as config from '../config'
import jwt from 'jsonwebtoken'
import { request } from 'https';
import User from '../model/user'

const logger = log4js.getLogger("authHandler")

export async function authHandler(req, res, next) {
    try {
        const authHeader = req.header('Authorization')
        if (authHeader) {
            const accessToken = authHeader.match(/Bearer (.*)/)[1]
            const decode = jwt.verify(accessToken, config.jwtSecretKey)
            req.userLogin = await User.getById(decode.userId)
            next()
        } else {
            const responseStatus = config.errorCode.API_E_0401
            res.status(401).json({ responseStatus })
        }

    } catch (error) {
        logger.error(error)
        const responseStatus = config.errorCode.API_E_0401
        res.status(401).json({ responseStatus })
    }
}

export async function permissionHandler(req, res, next) {
    logger.debug('userLogin',req.userLogin)
    if (req.userLogin && req.userLogin.role === 'admin') {
        next()
    } else {
        const responseStatus = config.errorCode.API_E_0401
        logger.error(responseStatus)
        res.status(401).json({ responseStatus })
    }
}

