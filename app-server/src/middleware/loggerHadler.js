
import log4js from '../logger'

const logger = log4js.getLogger("loggerHandler")

export function loggerReqHandler(req, res, next) {
    logger.debug("REQUEST",req.method, req.originalUrl,req.body)
    next()
}

export const loggerResHandler = log4js.connectLogger(logger, {
    level: log4js.levels.INFO, format: '[:method] [:url] [:response-time mSec] [:remote-addr] [:status] [:user-agent]'
})