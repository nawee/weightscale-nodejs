import log4js from '../logger'
import * as config from '../config'
import { APIError } from '../model/error'

const logger = log4js.getLogger("errorHandler")

export function bodyParserErrorHandler(err, req, res, next) {
    let responseStatus
    if (err instanceof APIError) {
        responseStatus = err.errorCode
    } else if (err instanceof SyntaxError) {
        if (err.type === 'entity.parse.failed') {
            responseStatus = config.errorCode.API_E_0001
        } else if (err.type === 'entity.too.large') {
            responseStatus = config.errorCode.API_E_0002
        }
    } else {
        responseStatus = config.errorCode.API_E_0000
    }
    logger.error(err)
    res.status(500).json({ responseStatus })

}