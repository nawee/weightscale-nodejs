import path from 'path'
import fs from 'fs'

// app config
export const appName = "weight-scale-nodejs"
export const server_port = process.env.SERVER_PORT? parseInt(process.env.SERVER_PORT): 8080
export const logDirectoryPath = process.env.LOG_DIRECTORY_PATH || path.join(__dirname, '..', 'logs')
export const logLevel = process.env.LOG_LEVEL || 'info'
export const errorCodeDirectoryPath = process.env.ERRORCODE_DIRECTORY_PATH || path.join(__dirname, '..', 'config/errorCode.json')
export const mongodbUri = process.env.MONGODB_URI
export const jwtSecretKey = process.env.JWT_SECRET_KEY
export const mqttUrl = process.env.MQTT_URL
export const mqttUsername = process.env.MQTT_USERNAME
export const mqttPassword = process.env.MQTT_PASSWORD

// endpoint

export const errorCode = JSON.parse(fs.readFileSync(errorCodeDirectoryPath, 'utf8'))
