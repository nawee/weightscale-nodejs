import log4js from '../logger'
import User from '../model/user'

const logger = log4js.getLogger("userController")

async function getAll(req, res, next) {
    try {
        const users = await User.getAll()
        logger.debug(users)
        res.status(200).json({ users })
    } catch (error) {
        next(error)
    }
}

async function getById(req, res, next) {
    try {
        const user = await User.getById(req.params.id)
        logger.debug(user)
        res.status(200).json({ user })
    } catch (error) {
        next(error)
    }
}

async function create(req, res, next) {
    try {
        const {username, password, role } = req.body
        const userToAdd = {
            username,
            password,
            role
        }
        const user = await User.add(userToAdd)
        res
            .header('Authorization',`Bearer ${User.genToken(user)}`)
            .status(200)
            .json({user})
    } catch (error) {
        next(error)
    }
}

async function update(req, res, next) {
    try {
        const userUpdate = {
            id: req.params.id,
            ...req.body
        }
        const user = await User.update(userUpdate)
        res.status(200).json({user})
    } catch (error) {
        next(error)
    }
}

async function remove(req, res, next) {
    try {
        await User.remove(req.params.id)
        res.status(200).end()
    } catch (error) {
        next(error)
    }
}


export default {
    getAll,
    getById,
    create,
    update,
    remove
}