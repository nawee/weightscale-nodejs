import userController from './userController'
import deviceController from './deviceController'
import dataController from './dataController'
import sessionController from './sessionController'
export default {
    userController,
    deviceController,
    dataController,
    sessionController
}