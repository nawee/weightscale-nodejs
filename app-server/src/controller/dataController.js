import log4js from '../logger';
import Data from '../model/data';

const logger = log4js.getLogger('dataController');

async function getAll(req, res, next) {
  try {
    const { page, limit, type, deviceIds } = req.body;
    const data = await Data.getAll(page, limit, type, deviceIds);
    logger.debug(data);
    res.status(200).json({ data });
  } catch (error) {
    next(error);
  }
}

async function getById(req, res, next) {
  try {
    const data = await Data.getById(req.params.id);
    logger.debug(data);
    res.status(200).json({ data });
  } catch (error) {
    next(error);
  }
}

async function create(req, res, next) {
  try {
    const { value, type, device } = req.body;
    const dataToAdd = {
      value,
      type,
      device
    };
    const data = await Data.add(dataToAdd);
    res.status(200).json({ data });
  } catch (error) {
    next(error);
  }
}

async function update(req, res, next) {
  try {
    const dataUpdate = {
      id: req.params.id,
      ...req.body
    };
    const data = await Data.update(dataUpdate);
    res.status(200).json({ data });
  } catch (error) {
    next(error);
  }
}

async function remove(req, res, next) {
  try {
    await Data.remove(req.params.id);
    res.status(200).end();
  } catch (error) {
    next(error);
  }
}

async function getStatusByDevice(req, res, next) {
  try {
    const data = await Data.getStatusByDevice(req.params.deviceId);
    res.status(200).json({ data });
  } catch (error) {
    next(error);
  }
}

export default {
  getAll,
  getById,
  create,
  update,
  remove,
  getStatusByDevice
};
