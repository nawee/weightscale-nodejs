import log4js from '../logger'
import Device from '../model/device'
import mqttClient from '../service/mqttClient'

const logger = log4js.getLogger("deviceController")

async function getAll(req, res, next) {
    try {
        const devices = await Device.getAll()
        logger.debug(devices)
        res.status(200).json({ devices })
    } catch (error) {
        next(error)
    }
}

async function getById(req, res, next) {
    try {
        const device = await Device.getById(req.params.id)
        logger.debug(device)
        res.status(200).json({ device })
    } catch (error) {
        next(error)
    }
}

async function create(req, res, next) {
    try {
        const { name, topic } = req.body
        const deviceToAdd = {
            name,
            topic
        }
        const device = await Device.add(deviceToAdd)
        res.status(200).json({ device })
    } catch (error) {
        next(error)
    }
}

async function update(req, res, next) {
    try {
        const deviceUpdate = {
            id: req.params.id,
            ...req.body
        }
        const device = await Device.update(deviceUpdate)
        res.status(200).json({ device })
    } catch (error) {
        next(error)
    }
}

async function remove(req, res, next) {
    try {
        await Device.remove(req.params.id)
        res.status(200).end()
    } catch (error) {
        next(error)
    }
}

function reboot(req, res, next) {
    const { topic } = req.query
    mqttClient.publish(topic, 'reboot;')
    res.status(200).end()
}

function setMini(req, res, next) {
    const { topic, value } = req.query
    mqttClient.publish(topic, `mini:${value};`)
    res.status(200).end()
}

function setDiff(req, res, next) {
    const { topic, value } = req.query
    mqttClient.publish(topic, `diff:${value};`)
    res.status(200).end()
}

function setSta(req, res, next) {
    const { topic, value } = req.query
    mqttClient.publish(topic, `sta:${value};`)
    res.status(200).end()
}

function refreshStatus(req, res, next) {
    const { topic } = req.query
    mqttClient.publish(topic, 'status;')
    res.status(200).end()
}

export default {
    getAll,
    getById,
    create,
    update,
    remove,
    reboot,
    setMini,
    setDiff,
    setSta,
    refreshStatus
}
