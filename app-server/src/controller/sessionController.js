import log4js from '../logger';
import * as config from '../config';
import User from '../model/user';

const logger = log4js.getLogger('sessionController');

async function login(req, res, next) {
  try {
    const { username, password } = req.body;
    const user = await User.getByUsername(username);
    const isValid = await User.verify(user, password);
    if (isValid) {
      logger.debug(user);
      res
        .header('Authorization', `Bearer ${User.genToken(user)}`)
        .status(200)
        .json({
          id: user.id,
          username: user.username,
          role: user.role
        });
    } else {
      const responseStatus = config.errorCode.API_E_0401;
      logger.debug(responseStatus);
      res.status(401).json({ responseStatus });
    }
  } catch (error) {
    next(error);
  }
}

export default {
  login
};
