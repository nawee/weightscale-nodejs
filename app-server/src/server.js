import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import "dotenv/config";
import * as config from "./config";
import routes from "./routes";
import log4js from "./logger";
import { loggerReqHandler, loggerResHandler } from "./middleware/loggerHadler";
import { bodyParserErrorHandler } from "./middleware/errorHandler";
import mqttClient from "./service/mqttClient";
import cors from "cors";

const logger = log4js.getLogger("server");
const app = express();
let server;

function setMidleware(app) {
  app.use(loggerReqHandler);
  app.use(loggerResHandler);
  app.use("/api", routes);

  app.use(bodyParserErrorHandler);
}
async function setup() {
  logger.debug("config value: ", config);

  await connectDb();
  mqttInit();
  mqttClient.connect();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json({ limit: "3mb" }));
  app.use(
    cors({
      allowedHeaders: ["Authorization", "Content-Type"],
      exposedHeaders: ["Authorization"]
    })
  );
  setMidleware(app);

  server = app.listen(config.server_port, () => {
    logger.info("Application listening on http port " + config.server_port);
  });
}

function connectDb() {
  return mongoose.connect(
    config.mongodbUri,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false
    },
    (err, result) => {
      if (err) {
        logger.error(err);
      } else {
        logger.info("mongo connected");
      }
    }
  );
}

function mqttInit() {}

function close() {
  server.close();
}

module.exports = {
  setup,
  app,
  close
};
