import mqtt from 'mqtt';
import log4js from '../logger';
import * as config from '../config';
import Device from '../model/device';
import Data from '../model/data';

const logger = log4js.getLogger('mqtt client');
let mqttClient = {};
let client;

mqttClient.connect = () => {
  client = mqtt.connect(
    config.mqttUrl,
    {
      clientId:
        'mqttjs_' +
        Math.random()
          .toString(16)
          .substr(2, 8),
      username: config.mqttUsername,
      password: config.mqttPassword
    }
  );
  client.on('connect', async () => {
    logger.info('mqtt connected');
    try {
      const devices = await Device.getAll();
      devices.forEach((device, index) => {
        mqttClient.subscribe(device.topic);
      });
      logger.debug('subscribe device', devices);
    } catch (error) {
      logger.error(error);
    }
  });

  client.on('message', (topic, message) => {
    try {
      subscribeHandler(topic, message.toString());
    } catch (error) {
      logger.error(error);
    }
  });

  client.on('error', error => {
    logger.error('connect error', error);
  });
};

mqttClient.subscribe = topic => {
  client.subscribe(topic, err => {
    if (err) {
      logger.error(err);
    }
  });
};

mqttClient.unsubscribe = topic => {
  client.unsubscribe(topic, err => {
    if (err) {
      logger.error(err);
    }
  });
};

mqttClient.publish = (topic, message) => {
  client.publish(topic, message);
};

async function subscribeHandler(topic, message) {
  logger.debug(topic, message);

  const { type, value } = isJSON(message)
    ? JSON.parse(message)
    : { type: 'command', value: message };

  const device = await Device.getByTopic(topic);
  if (!type) {
    return;
  }
  switch (type) {
    case 'weight':
      saveWeight(type, value, device);
      break;
    case 'status':
      saveStatus(type, value, device);
      break;
    case 'sensor':
      saveSensor(value, device);
      break;
    case 'command':
      saveCommand(type, value, device);
      break;
    default:
      break;
  }
}

function saveWeight(type, value, device) {
  const dataToAdd = {
    type,
    value: parseFloat(value),
    device: device.id
  };
  logger.debug('data to add', dataToAdd);
  Data.add(dataToAdd).then(result => {
    logger.debug('add success', result);
  });
}

function saveStatus(type, value, device) {
  const dataToAdd = {
    type,
    value,
    device: device.id
  };
  logger.debug('data to add', dataToAdd);
  Data.add(dataToAdd).then(result => {
    logger.debug('add success', result);
  });
}

function saveSensor(value, device) {
  const sensorTypes = [
    'temperature',
    'humidity',
    'pressure',
    'altitude',
    'light',
    'gas',
    'time'
  ];
  const sensorValue = value.split(',');
  sensorTypes.forEach((type, index) => {
    const dataToAdd = {
      type,
      value: parseFloat(sensorValue[index]),
      device: device.id
    };
    logger.debug(index, 'data to add', dataToAdd);
    Data.add(dataToAdd).then(result => {
      logger.debug('add success', result);
    });
  });
}

function saveCommand(type, value, device) {
  const dataToAdd = {
    type,
    value,
    device: device.id
  };
  logger.debug('data to add', dataToAdd);
  Data.add(dataToAdd).then(result => {
    logger.debug('add success', result);
  });
}

function isJSON(str) {
  try {
    return JSON.parse(str) && !!str;
  } catch (e) {
    return false;
  }
}

export default mqttClient;
