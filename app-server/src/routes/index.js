import express from 'express';
import ctrl from '../controller';
import { authHandler } from '../middleware/authHandle';

const router = express.Router();
// userController
router.get('/users', authHandler, ctrl.userController.getAll);
router.get('/user/:id', authHandler, ctrl.userController.getById);
router.post('/user', authHandler, ctrl.userController.create);
router.patch('/user/:id', authHandler, ctrl.userController.update);
router.delete('/user/:id', authHandler, ctrl.userController.remove);

// deviceController
router.get('/devices', authHandler, ctrl.deviceController.getAll);
router.post('/device', authHandler, ctrl.deviceController.create);
router.patch('/device/:id', authHandler, ctrl.deviceController.update);
router.delete('/device/:id', authHandler, ctrl.deviceController.remove);
router.get('/device/reboot', authHandler, ctrl.deviceController.reboot);
router.get('/device/mini', authHandler, ctrl.deviceController.setMini);
router.get('/device/diff', authHandler, ctrl.deviceController.setDiff);
router.get('/device/sta', authHandler, ctrl.deviceController.setSta);
router.get(
  '/device/refreshStatus',
  authHandler,
  ctrl.deviceController.refreshStatus
);
router.get('/device/:id', authHandler, ctrl.deviceController.getById);

// dataController
router.post('/datas', authHandler, ctrl.dataController.getAll);
router.get('/data/:id', authHandler, ctrl.dataController.getById);
router.post('/data', authHandler, ctrl.dataController.create);
router.patch('/data/:id', authHandler, ctrl.dataController.update);
router.delete('/data/:id', authHandler, ctrl.dataController.remove);
router.get(
  '/data/deviceStatus/:deviceId',
  ctrl.dataController.getStatusByDevice
);

// sessionController
router.post('/login', ctrl.sessionController.login);

router.use('*', function(req, res) {
  res.status(404).end();
});

export default router;
