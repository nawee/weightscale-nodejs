import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const DataSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  type: {
    type: String,
    required: true,
    enum: [
      'status',
      'weight',
      'temperature',
      'humidity',
      'pressure',
      'altitude',
      'light',
      'time',
      'gas',
      'command'
    ]
  },
  value: { type: mongoose.Schema.Types.Mixed },
  device: { type: mongoose.Types.ObjectId, ref: 'Device' },
  createDate: { type: Date, default: Date.now }
});
DataSchema.plugin(mongoosePaginate);

let model = mongoose.model('Data', DataSchema, 'Data');

model.getById = id => {
  return model.findById(id).then(data => ({
    id: data._id,
    type: data.type,
    value: data.value,
    device: {
      id: data.device._id,
      name: data.device.name,
      topic: data.device.topic
    },
    createDate: data.createDate
  }));
};

function getAllByType(page, limit, type) {
  const options = {
    sort: { createDate: -1 },
    populate: { path: 'device', select: '-__v' },
    page: parseInt(page) || 1,
    limit: parseInt(limit) || 20
  };
  return model.paginate({ type }, options).then(result => ({
    id: result._id,
    total: result.total,
    limit: result.limit,
    page: result.page,
    pages: result.pages,
    docs: result.docs.map(data => ({
      id: data._id,
      type: data.type,
      value: data.value,
      device: {
        id: data.device._id,
        name: data.device.name,
        topic: data.device.topic
      },
      createDate: data.createDate
    }))
  }));
}

function getAllByTypeAndDeviceIds(page, limit, type, deviceIds) {
  const options = {
    sort: { createDate: -1 },
    populate: { path: 'device', select: '-__v' },
    page: parseInt(page) || 1,
    limit: parseInt(limit) || 20
  };
  return model
    .paginate({ type, device: { $in: deviceIds } }, options)
    .then(result => ({
      id: result._id,
      total: result.total,
      limit: result.limit,
      page: result.page,
      pages: result.pages,
      docs: result.docs.map(data => ({
        id: data._id,
        type: data.type,
        value: data.value,
        device: {
          id: data.device._id,
          name: data.device.name,
          topic: data.device.topic
        },
        createDate: data.createDate
      }))
    }));
}

model.getAll = (page, limit, type, deviceIds) => {
  if (deviceIds.length) {
    return getAllByTypeAndDeviceIds(
      page,
      limit,
      type,
      deviceIds.map(ids => new mongoose.Types.ObjectId(ids))
    );
  } else {
    return getAllByType(page, limit, type);
  }
};

model.add = async data => {
  const { type, value, device } = data;
  const dataToAdd = new model({
    _id: new mongoose.Types.ObjectId(),
    type,
    value,
    device
  });
  return dataToAdd.save().then(data => ({
    id: data._id,
    type: data.type,
    value: data.value,
    device: {
      id: data.device._id,
      name: data.device.name,
      topic: data.device.topic
    }
  }));
};

model.update = data => {
  const { id, ...updateParams } = data;
  return model
    .findOneAndUpdate(
      { _id: id },
      { $set: updateParams },
      { new: true, runValidators: true }
    )
    .then(data => ({
      id: data._id,
      type: data.type,
      value: data.value,
      device: {
        id: data.device._id,
        name: data.device.name,
        topic: data.device.topic
      }
    }));
};

model.getStatusByDevice = deviceId => {
  return model
    .findOne({
      device: deviceId,
      type: 'status'
    })
    .sort({ createDate: -1 });
};

model.remove = id => {
  return model.findOneAndDelete({ _id: id });
};

export default model;
