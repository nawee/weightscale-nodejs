class BaseError extends Error {
    constructor(message, errorCode) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
        Error.captureStackTrace(this, this.constructor.name)
    }
}

class APIError extends BaseError {
    constructor(errorCode) {
        super('API Error', errorCode)
    }
}

module.exports = {
    APIError
}