import mongoose from 'mongoose'

const DeviceSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId },
    name: { type: String, required: true, unique: true },
    topic: { type: String, required: true, unique: true }
})

let model = mongoose.model('Device', DeviceSchema, 'Device')

model.getById = (id) => {
    return model.findById(id)
        .then(device => ({
            id: device._id,
            name: device.name,
            topic: device.topic
        }))
}

model.getAll = () => {
    return model.find()
        .then(devices => devices.map(device => ({
            id: device._id,
            name: device.name,
            topic: device.topic
        })))
}

model.getByTopic = (topic) => {
    return model
        .findOne({ topic })
        .then(device => ({
            id: device._id,
            name: device.name,
            topic: device.topic
        }))
}

model.add = (device) => {
    const { name, topic } = device
    const deviceToAdd = new model({
        _id: new mongoose.Types.ObjectId,
        name,
        topic
    })
    return deviceToAdd.save()
        .then(device => ({
            id: device._id
        }))
}

model.update = (device) => {
    const { id, ...updateParams } = device
    return model
        .findOneAndUpdate(
            { _id: id },
            { $set: updateParams },
            { new: true, runValidators: true }
        )
        .then(device => ({
            id: device._id,
            name: device.name,
            topic: device.topic
        }))
}

model.remove = (id) => {
    return model.findOneAndDelete({ _id: id })
}

export default model
