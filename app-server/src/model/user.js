import mongoose from 'mongoose';
import * as config from '../config';
import { APIError } from '../model/error';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const UserSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: {
    type: String,
    required: true,
    enum: ['user', 'admin'],
    default: 'user'
  }
});

UserSchema.post('findOne', (result, next) => {
  if (result == null) {
    return next(new APIError(config.errorCode.API_E_0404));
  }
  next();
});

let model = mongoose.model('User', UserSchema, 'User');

model.getById = id => {
  return model.findById(id).then(user => ({
    id: user._id,
    username: user.username,
    role: user.role
  }));
};

model.getAll = () => {
  return model.find().then(users =>
    users.map(user => ({
      id: user._id,
      username: user.username,
      role: user.role
    }))
  );
};

model.add = async user => {
  const { username, password } = user;

  const hash = await bcrypt.hash(password, 12);

  const userToAdd = new model({
    _id: new mongoose.Types.ObjectId(),
    username,
    password: hash,
    role: 'user'
  });
  return userToAdd.save().then(user => ({
    id: user._id,
    username: user.username,
    role: user.role
  }));
};

model.update = user => {
  const { id, ...updateParams } = user;
  return model
    .findOneAndUpdate(
      { _id: id },
      { $set: updateParams },
      { new: true, runValidators: true }
    )
    .then(user => ({
      id: user._id,
      username: user.username,
      role: user.role
    }));
};

model.remove = id => {
  return model.findOneAndDelete({ _id: id });
};

model.isAdmin = role => {
  if (role !== 'admin') {
    throw new APIError(config.errorCode.API_E_0401);
  }
};

model.genToken = user => {
  return jwt.sign({ userId: user.id }, config.jwtSecretKey, {
    expiresIn: '1h'
  });
};

model.getByUsername = username => {
  return model.findOne({ username }).then(user => ({
    id: user._id,
    username: user.username,
    password: user.password,
    role: user.role
  }));
};

model.verify = async (user, password) => {
  const hash = user.password;
  const isValid = await bcrypt.compare(password, hash);
  return isValid;
};

export default model;
