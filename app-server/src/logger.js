import * as config from './config'
import path from 'path'
import log4js from 'log4js'

log4js.configure({
    appenders: {
        file: {
            type: 'dateFile',
            filename: path.join(config.logDirectoryPath, config.appName + '.log'),
            pattern: '.yyyy-MM-dd',
            layout: {
                type: 'pattern',
                pattern: '[%d] [%h] [%z] [%p] [%c] %m',
            },
            compress: true
        },
        console: { 
            type: 'stdout', 
            layout: {
                type: 'pattern',
                pattern: '%[[%d] [%h] [%z] [%p] [%c]%] %m',
            }
        }
    },
    categories: {
        default: { appenders: ['console', 'file'], level: config.logLevel }
    }
})

export default log4js 
