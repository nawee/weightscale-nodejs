db.createUser({user: 'admin',pwd: 'P@ssw0rd',roles: [ { role: 'root', db: 'admin' } ]})
db.createUser({user: 'user',pwd: 'P@ssw0rd',roles: [{ role: 'readWrite', db: 'dataDb' }]})
mongo -u admin --authenticationDatabase admin
mongo -u user --authenticationDatabase dataDb
use dataDb
db.listCollections()
db.dataDb
data.Device