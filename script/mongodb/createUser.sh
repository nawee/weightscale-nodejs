db.User.insertMany([
  { 
    "role" : "admin",
    "username" : "admin",
    "password" : "$2b$12$iVHTTEtqnokE/xVKCDuXAubngic/CJkrng9Aba.SUWaJ3HR/KGKEm" 
  },
  { 
    "role" : "user",
    "username" : "user",
    "password" : "$2b$12$3FFe6/dEFnVGDHOnwO98T.0OeXR60lZwNQX7iyX4/K4VJnp32tKIa"
  }
])
